

export default function Banner() {

    return(
        <div className="jumbotron jumbotron-fluid m-2">
            <div className="container">
                <h1 className="display-5">Zuitt Coding Bootcamp</h1>
                <p className="lead">Opportunities for everyone,</p>
                <a className="btn btn-info">Enroll now</a>
             </div>
        </div>

    )
}